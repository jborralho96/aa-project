#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:05:13 2017

@author: nuno
"""

import numpy as np
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors.kde import KernelDensity
from sklearn.cross_validation import KFold
import matplotlib.pyplot as plt


mat = np.loadtxt('TP1-data.csv',delimiter=',')


#KNN
def calc_foldKNN(feats, X,Y, train_ix,valid_ix,k):
    """return error for train and validation sets"""
    reg = KNeighborsClassifier(n_neighbors=k)
    reg.fit(X[train_ix,:feats],Y[train_ix])
    #prob = reg.predict_proba(X[:,:feats])[:,1]
    #squares = (prob-Y)**2
    return 1-reg.score(X[train_ix,:], Y[train_ix]),1-reg.score(X[valid_ix,:], Y[valid_ix])


def knn():
    data = shuffle(mat,random_state=2017)

    Ys = data[:,-1]
    Xs = data[:,:-1]
    
    means = np.mean(Xs,axis=0)
    stdevs = np.std(Xs,axis=0)
    Xs = (Xs-means)/stdevs
    
    X_r,X_t,Y_r,Y_t = train_test_split(Xs,Ys, test_size=0.33,stratify = Ys, random_state=2017)
    folds = 5
    kf = KFold(len(Y_r), n_folds=folds)
    erros_tr=[]
    erros_va=[]
    k=1;
    for feats in range(1,20):
        tr_err = va_err = 0
        for tr_ix,va_ix in kf:
            r,v = calc_foldKNN(4,X_r,Y_r,tr_ix,va_ix,k)
            tr_err += r
            va_err += v
        k=k+2
        erros_tr.append(tr_err/folds)
        erros_va.append(va_err/folds)
        
    erros_tr = np.array(erros_tr)
    erros_va = np.array(erros_va)    
    
    plt.figure(figsize=(13,8), frameon= None)
    plt.plot(range(1,20),erros_tr,'-')
    plt.plot(range(1,20),erros_va, '-')
    
    #plt.show()
    plt.savefig('chart.png',dpi=300)
    
    #true error
    # k equals 1,3 or 5??
    best_index=np.argmin(erros_va)
    k=best_index*2+1
    print('best k:', k)
    reg = KNeighborsClassifier(n_neighbors=k)
    reg.fit(X_r,Y_r)
    #print("error:",1-reg.score(X_t, Y_t))
    X_t, Y_t
    c=reg.predict(X_t)

    
    return c,Y_t



#Logistic Regression

def calc_foldLR(feats, X,Y, train_ix,valid_ix,C=1e12):
    """return error for train and validation sets"""
    reg = LogisticRegression(C=C, tol=1e-10)
    reg.fit(X[train_ix,:feats],Y[train_ix])
    prob = reg.predict_proba(X[:,:feats])[:,1]
    squares = (prob-Y)**2
    return np.mean(squares[train_ix]),np.mean(squares[valid_ix])


def lr():
    data = shuffle(mat,random_state=2017)

    Ys = data[:,-1]
    Xs = data[:,:-1]
    
    means = np.mean(Xs,axis=0)
    stdevs = np.std(Xs,axis=0)
    Xs = (Xs-means)/stdevs
    
    X_r,X_t,Y_r,Y_t = train_test_split(Xs,Ys, test_size=0.33,stratify = Ys,random_state=2017)
    
    folds = 5
    kf = KFold(len(Y_r), n_folds=folds)
    erros_tr=[]
    erros_va=[]
    c=1;
    min_err=1
    best_c = 1
    for feats in range(1,20):
        tr_err = va_err = 0
        for tr_ix,va_ix in kf:
            r,v = calc_foldLR(4,X_r,Y_r,tr_ix,va_ix,c)
            tr_err += r
            va_err += v
        if va_err/folds < min_err:
            best_c=c
            min_err=va_err/folds
        c=c*2
        erros_tr.append(tr_err/folds)
        erros_va.append(va_err/folds)
    #print(feats,':', tr_err/folds,va_err/folds)
    
    erros_tr = np.array(erros_tr)
    erros_va = np.array(erros_va)    
    
    

    plt.figure(figsize=(13,8), frameon= None)
    plt.plot(range(1,20),erros_tr,'-')
    plt.plot(range(1,20),erros_va, '-')
    
    #plt.show()
    plt.savefig('chart.png',dpi=300)
    
    
    #calculate true error
    best_index=np.argmin(erros_va)
    c=best_c
    print('best index ', best_index)
    print('best c:', c)
    reg = LogisticRegression(C=c,tol=1e-10)
    reg.fit(X_r,Y_r)
    1-reg.score(X_t, Y_t)
    X_t, Y_t
    c=reg.predict(X_t)
    
    return c,Y_t


#Naive Bayes

def calcKDE(X, Y, bw, classe, nbfeutures):
    d=X[Y==classe,:]
    kde=[]
    for i in range(0,nbfeutures):
        kde.append(KernelDensity(bandwidth=bw).fit(d[:,[i]]))
    return kde

def fitNB(nbfeatures, X, Y, bw, classes):
    kde=[]
    for i in range(0, len(classes)):
        kde.extend(calcKDE(X, Y, bw, classes[i], nbfeatures) )
    return kde

def scoreNB1(X, Y, classes, nbfeatures,kde):
    scores=np.zeros((len(X[:,0]),len(classes)))
    classificados=np.zeros((len(X[:,0]),1))

    for j in range(0, len(classes)):
        scores[:,j]=np.log(len(X[Y==classes[j],0]/len(X[:,0])))
        for k in range(0, nbfeatures):
            scores[:,j]+=kde[k+(j*nbfeatures)].score_samples(X[:,[k]])
    for i in range(0, len(X[:,0])):
        classificados[i]=classes[np.argmax(scores[i])]

    error=np.sum(Y==classificados.reshape(1,len(classificados)))/len(Y)

    return error, classificados

def calc_foldNB(feats, X,Y, train_ix,valid_ix, bw):
    """return error for train and validation sets"""
    classes=[0,1]
    kde = fitNB(4, X[train_ix,:],Y[train_ix], bw, classes)
    acc_t, c = scoreNB1(X[train_ix,:], Y[train_ix],classes,4,kde)
    acc_v, c = scoreNB1(X[valid_ix,:], Y[valid_ix],classes,4,kde)
    return 1-acc_t,1-acc_v


def nb():
    data = shuffle(mat,random_state=2017)

    Ys = data[:,-1]
    Xs = data[:,:-1]
    
    X_r,X_t,Y_r,Y_t = train_test_split(Xs,Ys, test_size=0.33,stratify = Ys,random_state=2017)
    folds = 5
    kf = KFold(len(Y_r), n_folds=folds)
    erros_tr=[]
    erros_va=[]
    for i in np.linspace(0.01,1,num=50):
        tr_err = va_err = 0
        for tr_ix,va_ix in kf:
            #print(tr_ix, ':', va_ix)scoreNB
            r,v = calc_foldNB(4,X_r,Y_r,tr_ix,va_ix,i)
            tr_err += r
            va_err += v
        erros_tr.append(tr_err/folds)
        erros_va.append(va_err/folds)
        #print('bw:',i)
        
    erros_tr = np.array(erros_tr)
    erros_va = np.array(erros_va)
    
    plt.figure(figsize=(13,8), frameon= None)
    plt.plot(range(1,51),erros_tr,'-')
    plt.plot(range(1,51),erros_va, '-')
    
    #plt.show()
    plt.savefig('chart.png',dpi=300)
    
    #calculate true error
    best_index=np.argmin(erros_va)
    print('bw:',best_index*0.02+0.01)
    bw=best_index*0.02+1
    
    kde = fitNB(4, X_r,Y_r, bw, [0,1])
    err,c= scoreNB1(X_t, Y_t,[0,1],4,kde)
    print(1-err)
    c=c.reshape(1,(len(X_t[:,0])))[0]
    
    return c,Y_t

def mcNemar(X,Y):
    up = (abs(X-Y) -1)**2
    down = X+Y
    return up/down

def compare(c1,c2,y):
    e01=0 #c1
    e10=0 #c2
    for i in range(0,len(y)):
        if(c1[i] == y[i]):
            evaluateC1 = True
        else:
            evaluateC1 = False
        
        if(c2[i] == y[i]):
            evaluateC2 = True
        else:
            evaluateC2 = False
            
        if(evaluateC1 and not evaluateC2):
            e10+=1
        if(not evaluateC1 and evaluateC2):
            e01+=1
            
    
            
    return e01,e10


knnC, Y_t = knn()

lrC,Y_t = lr()

nbC,Y_t = nb()

knnCorrect = np.sum(knnC == Y_t)
print('knn correct :',knnCorrect, ' knn wrong:', len(Y_t) - knnCorrect)
lrCorrect = np.sum(lrC == Y_t)
print('lr correct :',lrCorrect, ' lr wrong:', len(Y_t) - lrCorrect)
nbCorrect = np.sum(nbC == Y_t)
print('nb correct :',nbCorrect, ' nb wrong:', len(Y_t) - nbCorrect,'\n')

e01lr,e10knn = compare(lrC,knnC,Y_t)
print('e01lr :',e01lr, ' e10knn:', e10knn)
print('Logistic Regression vs KNN: ', mcNemar(e01lr,e10knn))


e01knn,e10nb = compare(knnC,nbC,Y_t)
print('e01knn :',e01knn, ' e10nb:', e10nb)
print('KNN vs Naive Bayes: ', mcNemar(e01knn,e10nb))

e01nb,e10lr = compare(nbC,lrC,Y_t)
print('e01nb :',e01nb, ' e10lr:', e10lr)
print('Naive Bayes vs Logistic Regression: ', mcNemar(e01nb,e10lr))
    

