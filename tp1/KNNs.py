# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 08:29:28 2017

@author: j.borralho
"""

import numpy as np
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import KFold
import matplotlib.pyplot as plt

mat = np.loadtxt('TP1-data.csv',delimiter=',')
data = shuffle(mat)
Ys = data[:,-1]
Xs = data[:,:-1]

means = np.mean(Xs,axis=0)
stdevs = np.std(Xs,axis=0)
Xs = (Xs-means)/stdevs

X_r,X_t,Y_r,Y_t = train_test_split(Xs,Ys, test_size=0.33,stratify = Ys)

def calc_fold(feats, X,Y, train_ix,valid_ix,k):
    """return error for train and validation sets"""

    reg = KNeighborsClassifier(n_neighbors=k)
    reg.fit(X[train_ix,:feats],Y[train_ix])
    #prob = reg.predict_proba(X[:,:feats])[:,1]
    #squares = (prob-Y)**2
    return 1-reg.score(X[train_ix,:], Y[train_ix]),1-reg.score(X[valid_ix,:], Y[valid_ix])

folds = 5
kf = KFold(len(Y_r), n_folds=folds)
erros = np.zeros((20,2))
erros_tr=[]
erros_va=[]
k=1;
for feats in range(1,20):
    tr_err = va_err = 0
    for tr_ix,va_ix in kf:
        r,v = calc_fold(4,X_r,Y_r,tr_ix,va_ix,k)
        tr_err += r
        va_err += v
    k=k+2
    erros_tr.append(tr_err/folds)
    erros_va.append(va_err/folds)
    
erros_tr = np.array(erros_tr)
erros_va = np.array(erros_va)    


legends=[]
plt.figure(figsize=(13,8), frameon= None)
plt.plot(range(1,20),erros_tr,'-')
plt.plot(range(1,20),erros_va, '-')

plt.show()
plt.savefig('chart.png',dpi=300)

#true error
# k equals 1,3 or 5??
k=1
reg = KNeighborsClassifier(n_neighbors=k)
reg.fit(X_r,Y_r)
print("error:",1-reg.score(X_t, Y_t))
X_t, Y_t


