#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 18:18:17 2017

@author: nuno
"""

import numpy as np
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split
from sklearn.neighbors.kde import KernelDensity
import matplotlib.pyplot as plt
from sklearn.cross_validation import KFold

def frange(x, y, jump):
  while x < y:
    yield x
    x += jump

mat = np.loadtxt('TP1-data.csv',delimiter=',')
data = shuffle(mat)
Ys = data[:,-1]
Xs = data[:,:-1]

X_r,X_t,Y_r,Y_t = train_test_split(Xs,Ys, test_size=0.33,stratify = Ys)

def calcKDE(X, Y, bw, classe, nbfeutures):
    d=X[Y==classe,:]
    kde=[]
    for i in range(0,nbfeutures):
        kde.append(KernelDensity(bandwidth=bw).fit(d[:,[i]]))
    return kde

def fitNB(nbfeatures, X, Y, bw, classes):
    kde=[]
    for i in range(0, len(classes)):
        kde.extend(calcKDE(X, Y, bw, classes[i], nbfeatures) )
    return kde

def scoreNB(X, Y, classes, nbfeatures, kde):
    scores=np.zeros((len(X[:,0]),len(classes)))
    classificados=np.zeros((len(X[:,0]),1))

    for i in range(0, len(X[:,0])):
        for j in range(0, len(classes)):
            scores[i,j]=np.log(len(X[Y==classes[j],0]/len(X[:,0])))
            for k in range(0, nbfeatures):
                scores[i,j]+=kde[k+(j*nbfeatures)].score(X[i,k])
        classificados[i]=classes[np.argmax(scores[i])]
    error = np.sum(Y==classificados.reshape(1,len(classificados)))/len(classificados)

    return error, classificados

def scoreNB1(X, Y, classes, nbfeatures,kde):
    scores=np.zeros((len(X[:,0]),len(classes)))
    classificados=np.zeros((len(X[:,0]),1))

    for j in range(0, len(classes)):
        scores[:,j]=np.log(len(X[Y==classes[j],0]/len(X[:,0])))
        for k in range(0, nbfeatures):
            scores[:,j]+=kde[k+(j*nbfeatures)].score_samples(X[:,[k]])
    for i in range(0, len(X[:,0])):
        classificados[i]=classes[np.argmax(scores[i])]

    error=np.sum(Y==classificados.reshape(1,len(classificados)))/len(Y)

    return error, classificados

def calc_fold(feats, X,Y, train_ix,valid_ix, bw):
    """return error for train and validation sets"""
    classes=[0,1]
    kde = fitNB(4, X[train_ix,:],Y[train_ix], bw, classes)
    print(kde)
    acc_t, c = scoreNB1(X[train_ix,:], Y[train_ix],classes,4,kde)
    acc_v, c = scoreNB1(X[valid_ix,:], Y[valid_ix],classes,4,kde)
    return 1-acc_t,1-acc_v


folds = 5
kf = KFold(len(Y_r), n_folds=folds)
erros = np.zeros((50,2))
erros_tr=[]
erros_va=[]
c=1;
best=1;
min_err = 1;
for i in np.linspace(0.01,1,num=50):
    tr_err = va_err = 0
    for tr_ix,va_ix in kf:
        #print(tr_ix, ':', va_ix)scoreNB
        r,v = calc_fold(4,X_r,Y_r,tr_ix,va_ix,i)
        tr_err += r
        va_err += v
    erros_tr.append(tr_err/folds)
    erros_va.append(va_err/folds)
    print('bw:',i)
erros_tr = np.array(erros_tr)
erros_va = np.array(erros_va)


legends=[]
plt.figure(figsize=(13,8), frameon= None)
plt.plot(range(1,51),erros_tr,'-')
plt.plot(range(1,51),erros_va, '-')

plt.show()
plt.savefig('chart.png',dpi=300)

#calculate true error
best_index=np.argmin(erros_va)
print('bw:',best_index*0.02+0.01)
bw=best_index*0.02+1

kde = fitNB(4, X_r,Y_r, bw, [0,1])
err,c= scoreNB1(X_t, Y_t,[0,1],4,kde)
print(1-err)
