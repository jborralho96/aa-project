# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 08:30:36 2017

@author: j.borralho
"""
import numpy
import matplotlib.pyplot as plt


fil = open('planets.csv')
lines = fil.readlines()
fil.close()

def planet_period(planet):
    """jambe is life
    """
    lines= open('planets.csv').readlines()
    for line in lines:
        parts = line.split(',')
        if parts[0] == planet:
            return float(parts[2])

def load_planet_data(file_name):
    """Return matrix with orbital radius and period"""
    rows = []
    lines = open(file_name).readlines()
    for line in lines[1:]:
        parts = line.split(',')
        rows.append((float(parts[1]),float(parts[2])) )
    return numpy.array(rows)

data = load_planet_data('planets.csv')


def data_as_si(planet_data):
    AU_METERS = 1.496e11
    YEAR_SECONDS = 3.16e7
    si_radii = planet_data[:,0] * AU_METERS
    si_periods = planet_data[:,1]* YEAR_SECONDS
    return si_radii, si_periods

def star_mass(file_name):
    G= 6.67e-11
    radii,periods = data_as_si(load_planet_data('planets.csv'))
    velocities = (2*numpy.pi*radii)/periods
    masses = velocities**2*radii/G
    return numpy.mean(masses), numpy.std(masses)
    
    
data = load_planet_data('planets.csv')
x = data[:,0]
y = data[:,1]
coefs = numpy.polyfit(x,y,2) #constroi o modelo
pxs = numpy.linspace(0,max(x),100)
poly = numpy.polyval(coefs,pxs) #modelo final

plt.figure()
plt.plot(x,y,'or')
plt.plot(pxs,poly,'-')
plt.title('Degree: 2')
plt.savefig('testplot.png')
plt.show()  
plt.close()

#wrong
def mass_son():
    rows = []
    lines = open('planets.csv').readlines()
    for line in lines[1:]:
        parts = line.split(',')
        v = (2*numpy.pi*(float(parts[1])*1.496e11))/(float(parts[2])*3.16e7)
        mSun = (v**2*float(parts[1]))/6.67e-11
        rows.append(mSun)
    return numpy.array(numpy.mean(rows))

