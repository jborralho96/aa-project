# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 08:26:39 2017

@author: j.borralho
"""
import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('bluegills.txt',skiprows=1,delimiter='\t') #salta a primeira linha e o separador dos dados
print(data.shape[0]) #linhas
print(data.shape[1]) #colunas

#normalizar os dados
means = np.mean(data,axis = 0)
print(means)
devs = np.std(data,axis = 0)
print(devs)
data = (data-means)/devs


# shuffle
ranks = np.arange(data.shape[0])
print(ranks)
np.random.shuffle(ranks)
data= data[ranks,:]

#split train, validation, test
half = int(data.shape[0]*0.5)
quarter = int(data.shape[0]*0.75)
train = data[:half,:]
valid = data[half:quarter,:]
test= data[quarter:,:]

plt.figure(figsize=(13,8), frameon= None)

pxs = np.linspace(min(data[:,0]), max(data[:,0]), 500)
best_error = 100000
legends=[]

def mean_square_error(data,coefs):
    pred = np.polyval(coefs,data[:,0]) #prever o valor a partir do modelo
    error = np.mean((data[:,1]-pred)**2)
    return error


for degree in range(1,6):
    coefs= np.polyfit(train[:,0], train[:,1], degree)
    train_error = mean_square_error(train, coefs) #so para o grafico
    valid_error = mean_square_error(valid, coefs)
    poly = np.polyval(coefs,pxs)
    legends.append("{}/{:3.4f}/{:3.4f}".format(degree,train_error,valid_error))
    plt.plot(pxs,poly)
    if valid_error < best_error:
        best_error = valid_error
        best_coef = np.copy(coefs)
        best_degree = degree
        
train_error = mean_square_error(train, best_coef)
test_error = mean_square_error(test, best_coef)

print(best_degree, test_error)

plt.legend(legends, bbox_to_anchor=(1,0.5))
plt.plot(train[:,0],train[:,1],'ob')
plt.plot(valid[:,0],valid[:,0],'og')
plt.plot(test[:,0], valid[:,0], 'or')
plt.title('blue quill size')
plt.savefog('chart.png',dpi=300)
    