# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#1ª c4arregar dados
#2ª shuffle ds dados
#3 separar as classes 
#4  standardizaçao das features



import numpy as np
from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import KFold
import matplotlib.pyplot as plt

mat = np.loadtxt('data.txt',delimiter=',')
data = shuffle(mat)
Ys = data[:,0]
Xs = data[:,1:]
means = np.mean(Xs,axis=0)
stdevs = np.std(Xs,axis=0)
Xs = (Xs-means)/stdevs

def poly_16features(X):
    """Expand data polynomially
    """
    X_exp = np.zeros((X.shape[0],X.shape[1]+14))
    X_exp[:,:2] = X 
    X_exp[:,2] = X[:,0]*X[:,1]
    X_exp[:,3] = X[:,0]**2
    X_exp[:,4] = X[:,1]**2
    X_exp[:,5] = X[:,0]**3
    X_exp[:,6] = X[:,1]**3
    X_exp[:,7] = X[:,0]**2*X[:,1]
    X_exp[:,8] = X[:,1]**2*X[:,0]
    X_exp[:,9] = X[:,0]**4
    X_exp[:,10] = X[:,1]**4
    X_exp[:,11] = X[:,0]**3*X[:,1]
    X_exp[:,12] = X[:,1]**3*X[:,0]
    X_exp[:,13] = X[:,0]**2*X[:,0]**2
    X_exp[:,14] = X[:,0]**5
    X_exp[:,15] = X[:,1]**5        
    return X_exp

Xs=poly_16features(Xs)
X_r,X_t,Y_r,Y_t = train_test_split(Xs, Ys, test_size=0.33,stratify = Ys)

def calc_fold(feats, X,Y, train_ix,valid_ix,C=1e12):
    """return error for train and validation sets"""
    reg = LogisticRegression(C=C, tol=1e-10)
    reg.fit(X[train_ix,:feats],Y[train_ix])
    prob = reg.predict_proba(X[:,:feats])[:,1]
    squares = (prob-Y)**2
    return np.mean(squares[train_ix]),np.mean(squares[valid_ix])

folds = 5
kf = KFold(len(Y_r), n_folds=folds)
erros = np.zeros((16,2))
erros_tr=[]
erros_va=[]
for feats in range(2,16):
    tr_err = va_err = 0
    for tr_ix,va_ix in kf:
        r,v = calc_fold(feats,X_r,Y_r,tr_ix,va_ix)
        tr_err += r
        va_err += v
    erros_tr.append(tr_err)
    erros_va.append(va_err)
    print(feats,':', tr_err/folds,va_err/folds)
    
erros_tr = np.array(erros_tr)
erros_va = np.array(erros_va)    


legends=[]
plt.figure(figsize=(13,8), frameon= None)
plt.plot(range(2,16),erros_tr,'-')
plt.plot(range(2,16),erros_va, '-')

plt.show()
plt.savefog('chart.png',dpi=300)

        
        
        
